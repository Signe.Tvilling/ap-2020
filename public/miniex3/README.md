MINIEX3

**Describe your throbber design, both conceptually and technically. What is your sketch? What do you want to explore and/or express?**

My sketch explores the original throbber experience, the impatience of encountering a throbber and the time aspect of reacting to one. At the same time, the throbber tries to explore how the computer process behind the throbber is best represented in movement. 
The throbber is 4 pink ellipses circulating around a middle. The circulation changes over time, the movement feels unpredictable. It both changes direction and how much it moves. The movement is supposed to symbolize the stream of data that the computer receives. This stream of data is not as predictable as a perfect round circle and an ellipse rotating in a fixed circle.
The light and the words inside the circle reflect the impatience that the user experiences when watching a throbber. The longer the user waits the more impatient the user becomes. First the light inside the throbber appears. This both reflect the data that the computer receives, but at the same time the core of impatience that the user experience. The words, appearing slowly reflects the accumulating impatience that at last explodes. 


**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your process of coding)?**

I have used the frameCount syntax inside 2 different if – statements. I used frameCount to create a progression in the program. Time is in this case constructed through the runnings of the program. The more times the program has run, the more time has passed. Also more functions has appeared. At the same time the frameCount is controlled by the syntax frameRate. The frameRate controls how many times the program should run and frameCount just counts it.  


**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a ticket transaction, and consider what a throbber communicates, and/or hides? How might we characterise this icon differently?**

The times i have experienced a throbber in action, is when something is not working. By now, we know technology as fast and functional. It is not the purpose that we have to wait, the purpose is that we click and the intended function happens. At the same time, the throbber becomes a way of giving the user feedback. The program is processing and working to load the action intended. So actually the throbber becomes a way for the computer to communicate to the user. As a form of communication, it is not very complete. When the throbber appearce we know that something is taking time, but we don’t know what is taking time. Is it the internet connection that fails? Is it the program that is really heavy? The throbber hides whats going on inside the computer, what barriers the computer has met trying to conduct our commands. It is a very powerless position to watch a throbber rotate, and it can feel a lot like wasting time. Therefor maybe the throbber should be fun and entertaining?  

![ScreenShot](Skærmbillede 2020-02-22 kl. 15.25.17.png)

**URL:**  https://signe.tvilling.gitlab.io/ap-2020/miniex3/

