
let grad = 0;
let diameter;
let angle = -4;
let numrotation = 50
function setup() {
  createCanvas(650, 450);
  print("hello world");
  frameRate(3);
  diameter = 20


  }

function draw() {
  background(252, 204, 236, 100);
  translate(width/2, height/2)
  scale(0.5); //to make the throbber the right size
  throbberrotation(); // a function i made myself
  fill(400,0,400);
  stroke(255,0,0,0);

//the glitter / light in the middle of the cirkle
  if (frameCount>40 && frameCount<400) {
translate(-width/2, -height/2) //had to make the translation minus becouse of the scale function used in draw
    fill(255);
    let d1 = 10 + (sin(angle) * diameter) / 2 + diameter / 2;
    let d2 = 10 + (sin(angle + PI / 2) * diameter) / 2 + diameter / 0.5;
    let d3 = 10 + (sin(angle + PI) * diameter) / 2 + diameter / 2;
    star(width/2+4, height/2, 3, d1, d1);
    star(width/2-7, height/2-3, 3, d1, d1);
    star(width/2+4, height/2+5, 3, d2, d2);
    star(width/2-4, height/2-5, 3, d2, d2);
    star(width/2, height/2-7, 3, d3, d3);
    star(width/2, height/2, 3, d3, d3);
   angle += 0.5;
 }
 //The text in the middle of the cirkel
textAlign(CENTER, CENTER);
textSize(30);
fill(50);
if (frameCount>100 && frameCount<130) {
  text("show", width/2, height/2);
}
  else if (frameCount>130 && frameCount<160) {
    text("me", width/2, height/2);
}
  else if (frameCount>160 && frameCount<190) {
textSize(55);
    text("NUDES", width/2, height/2);
}
//   else if (frameCount>190 && frameCount<220)  {
//     textSize(50)
//     fill(55)
//     //fill(237, 69, 237)
//     text("WEBPAGE", width/2, height/2);
// }
  else if (frameCount>220) {
    frameCount = 40

  }




function throbberrotation() {

push();
  let cir = grad/numrotation*(frameCount);
     grad = grad+10 // to make the changing rotating

  rotate(radians(cir));
  noStroke();
  fill(400,2,400);
  ellipse(60,0,50,5);
  ellipse(-60, -0,50,5);
  ellipse(170,0,150,5);
  ellipse(-170, -0,150,5);
  pop();


}
//The star function used to create the light in the middle of the cirkle
  function star(x, y, radius1, radius2, npoints) {
    let angle = TWO_PI / npoints;
    let halfAngle = angle / 2.0;
    beginShape();
    for (let a = 0; a < TWO_PI; a += angle) {
      let sx = x + cos(a) * radius2;
      let sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a + halfAngle) * radius1;
      sy = y + sin(a + halfAngle) * radius1;
      vertex(sx, sy);
    }
    endShape(CLOSE);
}
}
