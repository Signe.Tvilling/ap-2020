var karaokeNight = 0;
var amountOfKaraokeStars = 2;
var sippingDrink = 0;
var you = 57;
var inhibition = 7;

function preload() {
  lukasGrahamHits = loadJSON('text3.json');
  goodFriends = loadSound('drunkInProgramming.mp3');
  oneMoreDrink = loadImage('theBar.jpg');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(amountOfKaraokeStars);
  goodFriends = createAudio('drunkInProgramming.mp3');
  goodFriends.autoplay(true);
  image(oneMoreDrink, sippingDrink, sippingDrink, windowWidth, windowHeight);
}

function draw() {
  print(frameCount);
  image(oneMoreDrink, sippingDrink, sippingDrink, windowWidth, windowHeight);

  // drunkYou = frameCount;
  if (frameCount > you) {
    frameCount = you - inhibition
    karaokeNight++
  }

  var singAlong = lukasGrahamHits.drunkinthemorning[karaokeNight].line
    textAlign(CENTER);
    textSize(40);
    textStyle(BOLD)
    stroke(sippingDrink);
    strokeWeight(4);
    fill(10, 122, sippingDrink);
    text(singAlong, 550, 125);

}
