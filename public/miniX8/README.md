<h2>README AND PROGRAM BY JAKOB & SIGNE</h2> 

**Provide a title of your work and a short description of the work.**

**Title: DrunkAtKaraoke**


This work is playing with words and sound. The work is a classic karaoke program, where text appears on the screen as the singer is supposed to sing them out loud. This work, and the karaoke medium, makes the user open their own mouth and use their voice as a part of the work. This is not a silent program, but the program makes the user participate with their own voice as a part of the program. This also makes the program change according to who’s using it and singing with it. The program aligns machine voice and human voice, by making the two collaborate in a mutual singing session. At the same time, the program also tries to add an extra dimension to the karaoke fun. While singing, suddenly the second verse is distorted and showing wrong texts. The text shown in the second verse is a critical comment on the karaoke medium, and the user that just sings words out loud, without thinking about what's actually being said and articulated. When singing karaoke the singer stops thinking about the words, and thereby uses one's own voice uncritically.


**Describe your program in terms of how it works, and what you have used and learnt?**

The program is built on the idea of a karaoke machine. In that sense, we wanted to make text appear and disappear on the canvas, timed to the rhythm of the music like karaoke machines usually does. We then made a JSON file that has all the lines of the lyrics inside of it. Each of the lines is organized in an array that starts from 0. To make each line appear on the canvas, we needed to make an if-statement that adds 1 to the array number so that only one lyrics line is shown on the canvas, and the line is changing every 3,5 second. To do that, we set the frameRate = 2 to make the program run the function draw() 2 times pr. second. When the function draw() has run 57 times, the frameCount will be reset to 50 and the array number will be added by 1; if the number was 2 before it will be changed to 3 and so on. The lyrics line with the array number that the program has reached will be displayed on the canvas and every 3,5 second the line will change to the next line. When the program is runned, the mp3 file, which is “Drunk In The Morning” by the danish band Lukas Graham, starts as well. The picture that the band uses on their cover of their first album is displayed on the canvas as well. Other than that, we have made various variables to make the code more interesting to read and by that make the code itself more artistic. We wanted to change the name of frameCount from “frameCount” to “drunkYou” but we had some issues in changing this. Instead we commented this variable above the if-statement, so the reader himself/herself can read “frameCount” as “drunkYou”. 


**Analyze and articulate your work**

The project is as mentioned a karaoke program, where the user is supposed to sing along to the music playing a specific song. The source code behind is supposed to represent in what settings karaoke might happen. When reading the source code, it describes all the elements one needs on a karaoke night. When signing karaoke, the aim is for the singer to really empathise with the song, and by doing that, we sometimes forget what we are saying and which words, voices and meaning we are extending out into the room and world;

    "His concern was to examine in what ways to say something is to do something, how in saying something we do something, 
    and how by saying something we do something" ( Speaking Code, 2013, p. 35)
         
This quote explains the fact that saying something, using one’s own voice, will have an effect on the ones listening. When singing, assumptions and prejudices may lie beneath the words we are singing. By screaming them out into a karaoke bar, we connect ourselves to these, and give them life. This does not mean that we should stop singing karaoke, but as the program encourages, we should have an extra thought on our own voice and how we are using it, and who is using it. The program encourages reflection in the second verse, where the lyrics are replaced with reflective questions, asking if the singer has considered their role in the program and their use of voice in singing out loud. 


The source code, which is a second notation for this program, tries to tie the primary notation, which is what the user sees, the program that is being run, and the secondary notation together. We made this attempt to tie these two together, to enhance the quality and material of the language in the source code. Writing a source code that is primarily for the program to run, can be reflected on by thinking of how we read the code. Both how we read and write the code, is a reflective process, to use the language to produce. This program attempts to use the qualities of language to enhance what the runnable program does and tackle; a karaoke night. By working on the source code, and more than just code, as language, we tie the computer and the human language together. Normally the interpretation of the program lies in what we see, but by giving the source code more value by adding a poetic aspect, the interpretation suddenly lies in the combination of what we see on the screen and what we read in the source code. 

    “Wall was emphasizing the point that code has expressive qualities, and the need for programmers to 
    “express both their emotional and technical natures simultaneously.””(Speaking Code, 2013, p. 24)
          
Making a source code with more poetic qualities, makes no difference to the computer, computers do not understand these qualities and building poetry into the code, only makes a difference for humans, and not computers. 

    “Much of the meaning of the program and how it might be understood is simply ignored by the computer.”(Speaking Code, 2013, p. 24) 


We have before touched upon the role and use of human voice in this analysis, how we use our voice and who/what makes us use it this way.  We have tried to embody the computer silently in the program. We took away the speech from the song and only left the words on the screen. Thereby inviting the user to apply speech and voice. We only gave the computer a voice by making words appear as if they came from the computer. Questions and reflections appear on the screen, as if it was the computer asking them.

    “It seems that the computer thinks and asks these questions, but in order for a computer to 
    duplicate actual thought, it would also need to reproduce consciousness itself.” (Speaking Code, 2013, p. 32)

 ![ScreenShot](screenminix8.png)

**URL:**  https://signe.tvilling.gitlab.io/ap-2020/miniX8/


