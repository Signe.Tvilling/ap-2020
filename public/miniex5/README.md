<h3>README.md 5</h3> 


•  **what is the departure point?**

I started making a throbber expressing the impatience that the user feels when encountering a throbber. I did that by playing with the frameCount syntax and making text appear on the screen. I also tried to make a comment about the data flow happening when a throbber rotates. The data flow is not being rendered in a steady and even way as the normal throbber rotates, but in a messing unpredictable way. Therefor my previous throbber rotated in a unpredictable way. 

•  **what do you want to express?**

This time, I tried to improve the throbber to express the data flow behind a throbber, that we as users do not see. Furthermore, I tried to express how the data capturing is continuing while watching a throbber, and how this data is travelling into the web. 

•  **what have you changed and why?**

I actually changed a lot. I wanted started out changing the canvas by adding the 3d WEBGL. Next I changed the shapes rotating so it became a rotating TV. I wanted to activate the webcam and make it the texture of the box illustrating the TV, but I could not make it work. I wanted the webcam to be the texture of the TV, to create a paradox of the user wanting to watch the screen, but actually the screen watching the user, as if the computer is watching us as TV. Instead of the webcam I added an illustration of an eye to the texture of the box, this also represent the watching a bit. To emphasize the data capturing and the ‘watching as if the computer was watching TV’ I used the mouseIsPressed function to add some for loops. I added two loops, of the same TV box, rotating around the X-axis. This was supposed to show the dataflow behind a throbber, where data is processed and incoming in a messier way than a normal throbber represents. Also, this represent how the data the computer collects are travelling out into the web and disappearing in the data flow.


•  **Reflect upon what Aesthetic Programming might be. Why program? 
 What does it mean by programming as a practice, or even as a method for design?**
 
Programming is more than numbers and commands. Programming is what forms our past, presence and future. Programming and the matter of who is programming is a cultural matter. The culture on the web is based on the infrastructure that the software and programs allow. We behave by the opportunities that we are given. Aesthetic programming is expanding programming to include the cultural and critical matter of programming, this is not about being good at numbers and code, but also to be able to think through code and reflect on the codes and software already made, rather than making a super complicated algorithm. The ability to program is in aesthetic programing also about exploring the digital culture.

Being able to code and program has many both cultural and personal benefits. Nick Montfort precents 5 different arguments that all humanists chould learn to program, and explains what good programming does. The first argument is that programming is proved to improve our cognitive abilities, Programming can help us think in new ways, improve our ability to think logically, planning and help our ability to problem solve. Next he argues that programming helps us built things. That we by learning this new tool of creating, we are able to help built and decide what programs should be made. This leads on to the second last argument, that programming gives us insight into cultural systems. The ability to code helps us understand and comment on already existing computer programs. He argues that for people who understand code, the world reveals itself as a series of decisions made by planners and designers on how the rest of us should live (page 273). Having knowledge about programming makes us able to answer questions about the things we know and the development of the world.  Computation can therefor help us built a better world, where people is more able to discuss and model social and political aspects of life. Last but not least he also recognizes that programming can be fun and creative, and that this clearly also is a good reason for people to learn. 


**Can you draw and link some of the concepts in the text and expand with your critical take?**

I personally clearly see the arguments for programming being a literacy, and that the world would be a better place, if everyone was able to program. But by taking this decision, of setting the ability to read and write as important as being able to program, we have as a society and world, taken a clear decision of how we see the future turn out. If programming languages should be a literacy, and everyone is able to program, we have chosen the digital future, where programming, software and computing is developed fast and by everyone. This means that computing will become a huge part of our lifes, and maybe rule the world? If we imagine a world where we use programming as often as we use letters and as we read anything, programming and computing must be everywhere. Also if everyone is programming, more complex and bigger programs will be created, do we really want to choose the digital world to be such a part of everyones life? 


**What is the relation between programming and digital culture?**

Programming is what decides out digital culture, it is the bricks and foundations of the possibilities we have online. It is what makes the structure and the economy of the internet. The structure of the web, can described through toughts by Femke Snelting in her text other geometries. Here she describes how the web can have different structures, according to different geometric shapes. The structure that we create the web around, is also crucial concerning what digital culture we live in. In Carolin Gerlitz and Anne Helmond’s article “The like economy: Social buttons and the data-intensive web”, they describe how the structure of the internet is connected through social buttons as the like button and other social plugins. This creates a digital culture and structure that are centralized around social platforms and mastodons deciding what digital culture we live by. 


 ![ScreenShot](skærmminiex5.png)

**URL:**  https://signe.tvilling.gitlab.io/ap-2020/miniex5/
