•	**Provide a title of your work and a short description of the work (within 1000 characters) as if you are submitting to the festival open call.**


**THIS IS ME TESTING YOU**

Our lives are more on the internet than we are aware of. The clicking of social buttons, is not just a measurement method and capturing of data, it is a capturing and datafication of our lives. We live our lives partly on the internet, making the data we produce valuable. By clicking one button, and especially the like button, we turn our small choices and opinions into data and are maybe unwillingly producing social data to be captured and used. 
The THIS IS ME TESTING YOU project concerns the data capturing of today’s web and our personal life. The project is a comment on the infrastructure that needs no more than a single like, to create a computer persona of the user. The project is supposed to show the user the amount of data that the like economy and on click on a virtual button can entail and how this data can be rendered and used. Maybe by facing one’s own computer self, we realize and start to deliberate how we should deal with the datafication of our lives. 


•	**Describe your program and what you have used and learnt.**

The program has a main focus on making things happen through the use of the DOM element; buttons. How we make a page change into something different through the click on a created and styled button. By making functions, and variables, that made it possible to have some syntax in setup, only being drawn once, and some syntax in draw, being drawn again and again, made it possible to have some text and buttons hide, and some move. The use of the buttons made a clear timeline and ‘train of events’ possible, making a program that was able to progress and become something new via interaction. 
I also explored the possibilities of the web cam and how I could intergrade and use this syntax and function in my work, broadening the syntax possibilities and actions. 

•	**Articulate how your program and thinking address the theme 'capture all'. What are the cultural implicatons of data capture?**

The program is a clear comment on the capture all infrastructure of today’s web. The program tries to address how easy it is for software to capture a lot
of data, through simple clicks on simple buttons. In making the program I was inspired by a quote from “The like economy: Social buttons and the data intensive web” by Carolin Gerlitz and Anne Helmond:
“An infrastructure that allows the exchange of data, traffic, affects, connections, and of course money, mediated through Social Plugins and most notably the Like button.” (1353)
The data that only the like button collects is massive, and this project tries to put that fact in focus by firstly asking the user to answer some questions, and afterwards only asks one simple silly question. But if the user answers this question, she is allowed to enter the webpage that shows the data that was collected by the pressing of that one like button. The data collected was enough and no other questions was needed. Entering the webpage shows how the computer sees the user, and how much data is collected. The user sees herself from the computer’s perspective, she sees the internet version of herself. She sees the data that is collected through the statements running down the screen to the left and sees how the data is rendered and used to the right. The data is used to figure out what the user ‘might buy’, by judging from the data collected by the clicking of the one silly like button. The user in this case is likely to buy Sing Star, and if the program was developed further, more products should be shown on this side of the screen. 
The program is supposed to confront the user with the information that is given away and collected so easily on the internet, and how we by living in the like economy world, is participating in the capturing of not just data, but the capturing and datafication of our lives. And by clicking these social button (e.g. the like button) we create an infrastructure where clicking is economic value. “Like economy which simultaneously enacts, measures and multiplies user actions.” (Gerlitz, Helmond, 1360). Buttons can hide information and it is sometimes hard to understand what we are consenting to by clicking. 

 
 ![ScreenShot](screenX4.png)

**URL:**  https://signe.tvilling.gitlab.io/ap-2020/minix4.1/
