MiniEx 1 

**1.	How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**
I think the most descriptive word I can use is confusing. I am left with 100 question to the code I just created myself. Why do I have to make semicolon? What is the clamps doing? What is the color-codes? And the most pressing one, why is it not working? 
Although I was left with all of these questions, it was not a bad experience, actually I found it very amusing sitting and trying to create something from nothing and continued to code right after the class. It sparks a creativity and curiosity in me, to know and create more. What happens if I write this command? Can I make an ellipse here? 

**2.	How is the coding process different from, or similar to, reading and writing text?**
I think the first experience with coding is very different from reading and writing. When reading and writing, the words is what you get. The words don’t mean anything else than the words. In programming the words and numbers are codes for other things. Images, colors, sounds. Therefor I think reading and writing is much easier to understand. But at the same time, it is the same procedure. To learn a set of symbols in a specific order that have a meaning, either represented in language or images etc. 

**3.	What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?**
Codes have for me been this unknown computer language that I honestly have been trusting blindly. I have only thought of the codes when something has not been working. The text written by Annette Vee helped me understand how big part of our culture and society programming and codes are. Arguing that learning to code is as important as learning to write and read, made me understand the importance of following the development and understanding technology as a student of digital design. The assigned readings also made me reflect on programming in a bigger picture. How different people have different motives for learning programming and giving others lessons in programming. When reading and writing became a literacy and was taught widely, different institutions had different motives for teaching people to read and write. Fx the church had a religious approach, and the Sovjet and the communists had a political agenda. Therefor we must reflect on our own learning, when learning this new language of programming and code. For what purpose are we learning, and how do we learn? 

**4.	Screenshot**
![ScreenShot](MiniEx1Screen.png)

**5.	URL**
RUNME: https://signe.tvilling.gitlab.io/ap-2020/MiniEx1-1/
