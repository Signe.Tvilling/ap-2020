  function setup() {
    createCanvas(720, 400);
    print("hello world");
  }

  function draw() {
    background(7, 118, 173);
    push();
    noStroke();
    fill("Yellow");
    translate(width * 0.5, height * 1);
    rotate(frameCount / 300.0);
    star(300, 30, 100, 50, 30);
    noStroke();
    fill(138, 134, 113);
    ellipse(-300, 30, 140, 140);
    fill(115, 111, 93);
    ellipse(-330, -5, 10, 40);
    ellipse(-300, 85, 45, 20);


    fill(82, 79, 66);
    ellipse(-300, -30, 30, 10);
    pop();

    noStroke();
    fill(240, 217, 43);
    square(340, 355, 50);
    fill(199, 86, 0);
    triangle(340,355,390,355,365,325);
    fill(110, 49, 3);
    rect(352,380,10,20);
  }

  function star(x, y, radius1, radius2, npoints) {
    let angle = TWO_PI / npoints;
    let halfAngle = angle / 2.0;
    beginShape();
    for (let a = 0; a < TWO_PI; a += angle) {
      let sx = x + cos(a) * radius2;
      let sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a + halfAngle) * radius1;
      sy = y + sin(a + halfAngle) * radius1;
      vertex(sx, sy);
    }
    endShape(CLOSE);
  }
