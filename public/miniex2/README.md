**Describe your program and what you have used and learnt.**

I have made 2 emojis discussing skin-color in emoji culture. The emojies are simple smiley-faces, but around the smiley-face rings of color appear. The difference between the two emojies are what color the smiley-face are and what color the rings are. Should the smiley start by being black or white? The syntax used to create the two emojies are ellipse, fill, nofill, arc, stroke and strokeWeight.  

**How would you put your emoji into a wider cultural context that concerns representations, identity, race, social, economics, culture, device politics and beyond?**

The emojies discuss the race-representation in the emoji culture. Why is it that emoji-coloring goes from white to black, and not from black to white? 
PS: i still cant get my RUNME link to work.. but nothing moves this time, so the picture will be enough. 

![ScreenShot](miniex2screen.png)

**URL**
https://signe.tvilling.gitlab.io/ap-2020/miniex2/
