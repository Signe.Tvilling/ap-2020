let buttonRed = 0;
let angle = 0;
let button1;
let button2;
let red = 136;
let green = 0;
let blue = 255;
let alpha = 150;
function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight, WEBGL);

}
function draw() {
  // basics
  background(230, 221, 220);
  angleMode(DEGREES);

  //the pot1
  push();
  translate(-750,-350);
  stroke(105, 105, 105);
  fill(163, 163, 163);
  strokeWeight(8);
  ellipse(300, 300, 300,300, 50);
  pop();

//the pot2
push();
strokeWeight(7);
stroke(105, 105, 105);
// line(-150, 100, -150, -100);
// line(200, 100, 200, -100);
translate(25,100);
cylinder(176, 1, 50);
pop();


push();
fill(105,105,105,100);
stroke(0.1);
translate(25, 10);
cylinder(176, 190, 25);
pop();

push();
translate(25,-85);
strokeWeight(1);
fill(130, 129, 129);
cylinder(176, 1, 50);
pop();

push();
noStroke();
fill(163, 163, 163);
// rect(-150, -100, 350, 200)
pop();


//Arrow structure
  push();
  scale(0.5);
  translate(130,-770);
  fill(0);
  rect(1010, 110, 25, 70);
  rect(1010, 200, 25, 70);
  triangle(985, 110, 1022, 80, 1060, 110)
  triangle(985, 270, 1022, 300, 1060, 270)
  pop();

//squares indicating speed without light
  push();
  translate(70,-400);
scale(0.5);
  rect(900, 400, 20, 20);
  rect(1000, 400, 20, 20);
  rect(1100, 400, 20, 20);
  pop();


//UP ARROW
push();
translate(-600,-400);
    button1 = createButton(' ');
    button1.position(1290,60);
    button1.style("padding","20px 6px");
    button1.style("color","#000000");
    button1.style("background","#000000");
    button1.style("border","none");
    button1.mousePressed(getRedUp);

// DOWN ARROW
    button2 = createButton(' ');
    button2.position(1290,110);
    button2.style("padding","20px 6px");
    button2.style("color","#000000");
    button2.style("background","#000000");
    button2.style("border","none");
    button2.mousePressed(getRedDown);
    pop();


//The red squares indicating speed
    if (buttonRed == 1 || buttonRed == 2 || buttonRed == 3) {
      push();
      //translate(-600,-400);
      fill(red,green,blue,alpha);
      translate(70,-400);
      scale(0.5);
      rect(900, 400, 20, 20);
      pop();
    }
    if (buttonRed == 2 || buttonRed == 3) {
      push();
      // translate(-600,-400);
      fill(red,green,blue,alpha);
      translate(70,-400);
    scale(0.5);
      rect(1000, 400, 20, 20);
      pop();
    }
    if (buttonRed == 3) {
      push();
      fill(red,green,blue,alpha);
      translate(70,-400);
      scale(0.5);
      rect(1100, 400, 20, 20);
      pop();
    }

    //The speed of the spoon
    if (buttonRed == 0) {
      push();
      stroke(red,green,blue);
      translate(-450,-50);
      rotate(-90);
      fill(red,green,blue,alpha);
      rect(0,0,10,145);

      pop();

      push();
      fill(red,green,blue,alpha);
      stroke(red,green,blue);
      translate(25, 100);
      triangle(0,0, 175, -100, 175, 0);
      pop();

    }
    if (buttonRed == 1) {
      push();
      translate(-450,-50);
      angle = angle+0.2;
      rotate(-90);
      rotate(angle);
      stroke(red,green,blue);
      fill(red,green,blue,alpha);
      rect(0,0,10,145);
      pop();

      push();
      stroke(200, 0, 0);
      translate(25, 100);
      angle = angle+0.2;
      rotateY(-angle);
      stroke(red,green,blue);
      fill(red,green,blue,alpha);
      triangle(0,0, 175, -100, 175, 0);
      pop();
    }
    if (buttonRed == 2) {
      push();
      translate(-450,-50);
      angle = angle+0.5;
      rotate(-90);
      rotate(angle);
      stroke(red,green,blue);
      fill(red,green,blue,alpha);
      rect(0,0,10,145);
      pop();

      push();
      stroke(200, 0, 0);
      translate(25, 100);
      angle = angle+0.5;
      rotateY(-angle);
      stroke(red,green,blue);
      fill(red,green,blue,alpha);
      triangle(0,0, 175, -100, 175, 0);
      pop();
    }
    if (buttonRed == 3) {
      push();
      translate(-450,-50);
      rotate(-90);
      angle = angle+1;
      rotate(angle);
      stroke(red,green,blue);
      fill(red,green,blue,alpha);
      rect(0,0,10,145);
      pop();

      push();
      stroke(200, 0, 0);
      translate(25, 100);
      angle = angle+1;
      rotateY(-angle)
      stroke(red,green,blue);
      fill(red,green,blue,alpha);
      triangle(0,0, 175, -100, 175, 0);
      pop();
    }
  }
  function keyPressed() {
    if (keyCode === 49){
      buttonRed = buttonRed+1;
    } if (keyCode === 50) {
      buttonRed = buttonRed-1;
    }
  }


  function getRedUp() {
  buttonRed = buttonRed+1
  }

  function getRedDown() {
    buttonRed = buttonRed-1
  }
