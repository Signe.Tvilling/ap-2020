let ylil=0;
let xlil=0;
let speedx = 1
let speedy = 1
let runningrect = [];
let numberrect = 0;

function setup() {
  // put setup code here
  createCanvas(800, 600);
    background(0);
    frameRate(50);
    //generating the squares in the array
    for (let i=0; i<numberrect; i++) {
    runningrect[i] = new Runningrect();
   }
 }

function draw() {
  //The original square moving in the beginning
  xlil = xlil+speedx
  if (xlil++) {
    ylil=ylil+speedy
    stroke(22, 204, 53)
    fill(0);
    rect(xlil,ylil,10,10);
  }

//these 4 if-statements makes the original square turn when hitting the sides of the canvas
if(xlil > width) {
  speedx = -4
  //generating a new rectangle to the array
  runningrect.push(new Runningrect());
  //variable used to allow the new squares to be generated and shown
  numberrect++
}
if(ylil<0) {
  speedy = 2
  runningrect.push(new Runningrect());
  numberrect++
}
if(ylil>600 ) {
  speedy = -6
  runningrect.push(new Runningrect());
  numberrect++

}
if (xlil<0) {
  speedx = 1
  runningrect.push(new Runningrect());
  numberrect++
}
//for loop generating and showing the squares
for(let i=0; i<numberrect; i++) {
  runningrect[i].move();
  runningrect[i].display();
  runningrect[i].directionShift();
}
//if-statement stopping the program when enough squares has been generated
if (numberrect>8000) {
  textSize(50);
  fill(0,255,0);
  textAlign(CENTER);
  text("I think you have Corona", width/2, height/2-75);
  noLoop(); //making to program stop looping
 }
}

//the class making the squares
class Runningrect {
 constructor() {
   this.x = random(0, 800);
   this.y = 0
   this.speedrectX = 3
   this.speedrectY = 3
 }

move() {
  this.x=this.x + this.speedrectX
  this.y=this.y + this.speedrectY
}

display() {
  fill(0);
  stroke(0,255,0);
  rect(this.x, this.y, 10, 10);
}

//the same as the previous 4 if-statements, just for the squares in the array
 directionShift() {
   if(this.x > width) {
     this.speedrectX = -3
     runningrect.push(new Runningrect());
     numberrect++
   }

   if(this.y<0) {
     this.speedrectY = 2
     runningrect.push(new Runningrect());
     numberrect++
   }

   if(this.y>600 ) {
     this.speedrectY = -6
     runningrect.push(new Runningrect());
     numberrect++
   }

   if (this.x<0) {
     this.speedrectX = 1
     runningrect.push(new Runningrect());
     numberrect++
   }
 }
}
