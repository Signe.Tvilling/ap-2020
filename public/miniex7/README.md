<h3>MiniEx 7 README</h3>

•	**What are the rules in your generative program and describe how your program performs over time and how are the running of the rules contingently enabled emergent behaviors?**

The rules of my program are simple:

1.	Every time the square(s) reaches the sides of the canvas, they turn direction.

2.	Every time the square(s) reaches the sides of the canvas and turn direction, a new square is generated. 

At the start of the program only one square appears on the screen. It moves across the screen until it hits the edge of the canvas, where (by the effects of an if-statement) it turns direction. Furthermore, another identical square appears on the top of the screen at a random x-position. Now there are two squares on the screen. When either of them hits one of the sides of the canvas, one more square is generated. This forces a chain reaction, making more and more squares appear on the screen. It quickly escalates and suddenly, after a very short amount of time, the whole screen is filled with squares, only generating even more squares.   

•	**What's the role of rules and processes in your work?**

The roles of the program are the ones determining what is possible in the program. The two simple rules determine what happens when loading the program. The rules make the program develop and auto-create, because the rules determine when something should change. It is what drives the program. The auto-generation and the creation of simple rules are a way of turning concepts into sequences that is possible to compute and program. 

These rules generate a chain reaction, where a simple situation escalates and develops to something uncontrollable. This is highly relevant at this very time. With the Corona virus invading the whole world, starting one place at one market in China, spreading uncontrollable throughout the country and now the world. When just one person is smitten, it quickly sparks a chain reaction of sickness. This program can be seen as a visualization of how quick an infection like the corona virus can spread and invade not only one country, but the whole world. Therefor I chose to call this program COVID-19 


•	**Draw upon the assigned reading(s), how does this mini-exericse help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter?**

When making an auto-generative system, we are creating software that are able to operate on their own and generate beyond our thoughts. We are creating a software that might act in ways that we do not expect and in ways we have not directly defined. This gives the creator of the program a new role. By making a program able to develop on its own in maybe unexpected ways, we are distancing ourselves from the work. Who are the author and creator of the final work? Is it the computer that created the specific artwork or is it the artist who wrote the code and instructions for the computer to follow? These new questions debating the role and relation of the machine and the creator emerges when making generative art. Maybe the answer to these questions is how we as individuals perceive and appreciate artworks and software, do we see the beauty in the final result, or do we see the beauty in the process of executing rules? 

 ![ScreenShot](screenminix7.png)

**URL:**  https://signe.tvilling.gitlab.io/ap-2020/miniex7/


