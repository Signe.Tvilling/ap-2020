<h2>README 10</h2>

I chose to revisit the game I made in miniX6 because this was the most complex program I have made. The thing that made it complex was the many different details and effects that I added to engage the player. Including sound effects, score and other features and details. The thing that was most complicated to describe in a flowchart was the classes and objects that I had created. It was hard to illustrate how the objects where deleted and added to the array of objects moving past the screen.  

Link to miniX6: https://gitlab.com/Signe.Tvilling/ap-2020/-/tree/master/public/Minix6

Link to the individual flowchart: https://gitlab.com/Signe.Tvilling/ap-2020/-/blob/master/public/MiniX10/InspectorFlow2.pdf

•  **What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?**

The challenge is to balance the two or to choose with focus the flowchart should have. At the level of communication, the flowchart can be used to communicate between partners and then the words and processed must be understandable to one that do not understand code language or lingo. Therefor the creators of the flowchart must ‘translate’ the algorithmic procedures into language that is non machine but instead human. In making a flowchart like this, the aim is that even your grandmother must be able to understand the procedure of the program. 

In making a flowchart that accounts for the complexity of an algorithm, the flowchart risks to become really long and twisting. The complexity is hard to grasp and capture in a flowchart without using terms from coding and computation, making the flowchart obscure for a non-programmer. The challenge is to simply grasp what the procedure and structure of the algorithm is, and capture it in as few and simple steps as possible. 


•  **What are the technical challenges for the two ideas and how are you going to address them?**

The technical challenge for the first idea is to extract data from our own program. We aim to use and criticize Google analytics, that are capturing a lot of user data, and give this captured data and information back to the user of the website. The challenge is to get an API from Google analytics that provide us with the data that they themselves capture. 

Link to the flowchart: https://gitlab.com/Signe.Tvilling/ap-2020/-/blob/master/public/MiniX10/Flow2.2.pdf

The other idea is based on the same idea of giving the user back the data that is being captured and displaying the captured data, making no secrets and complete awareness of the specific information that the user provides when using the website. This idea is based on the personal information that we show online. The user writes her own name into the program and her whole online personality and all information about her that she has given up online is shown before her eyes. Ideally the information would come from an API from Facebook or something similar, but this is out of our reach. 

Link to the flowchart: https://gitlab.com/Signe.Tvilling/ap-2020/-/blob/master/public/MiniX10/Flowchart1.1.pdf

•  **What is the value of the individual and the group flowchart that you have produced?**

The flowcharts created in corporation and the flowchart created individually have two completely different purposes. The flowchart we created together as a group was in this specific situation a form of communication. We used the flowchart to communicate and clarify our idea and the potential process behind. Before creating the flowchart, we brainstormed and agreed on two ideas we would continue to work on. Before we began to produce the flowcharts, we had a guiding conceptual idea, but while creating the flowchart we discussed the potential details and agreed on a draft to how this program would function. This also created a better understanding of what was possible and what structure the program should have.

When creating a flowchart on my own, the flowchart had another purpose. I did not use the flowchart to communicate or to document. This flowchart is made weeks after I completed the program. The flowchart in this case was more helpful in understanding the specific functioning and structure of the program that I created myself. In making the flowchart I explored and discovered the different loops and opportunities that I had given the computer. When was there more than one option and what exactly influenced the outcome?   
