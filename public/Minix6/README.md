<h3>INSPECTOR NORSE ESCAPING FROM EARTH</h3>

**Disclaimer!!:** the Inspector Norse song should play when opening the RUNME link but unfortunately it doesn't... (it does when i open the sketch in atom)?? If you know why it is not playing, then let me know!

•  **Describe how does your game/game objects work?**

I designed a game based on a song called “Inspector Norse” by Tod Terje. In this game, the player steers a picture of “inspector Norse” around the sky on his way escaping from the earth. Inspector Norse is fleeing from earth because the earth is on fire. He flies through the sky while trying to avoid the falling bombs. If Inspector Norse hits a bomb he loses one out of 3 lives. At the same time as trying to avoid the bombs, the player tries to catch the redbulls falling from the sky. The redbulls increases the score by 20 points. If the player loses all 3 lives, the game is over and Inspector Norse is dead. 

•  **Describe how you program the objects and their related attributes and methods in your game.** 

I created two classes of objects. One making the bombs and one making the redbulls. In each class I made a constructor, defining some variables. I made the properties of the object. I made the bombs with two simple shapes, using the variables I defined in the constructor. This made it possible for the bombs to fall from different points in the sky, and in different sizes. The redbulls is just a picture, but again a used variables from the constructor, to create the same properties as the bombs. 

I also created a ‘move’ element making the bombs and the redbulls move down the screen in different speeds. 

At last I created a delete element using the syntax “splice” deleting the objects when they exit the screen. I do this so it’s possible to always have 41 bombs on the screen, or 5 redbulls. 
I created an array for each class, and made for-loops creating endless bombs and redbulls. This made it possible to create a lot of bombs and a lot of redbulls, without 1000 lines of code. I created the bomb-array with 41 bombs. Every time a bomb exits the screen and is deleted, a new bomb is pushed into the array with the syntax “.push”, that is what makes the array endless. 


•  **What are the characteristics of object-oriented programming and the wider implications of abstraction?**

Object-oriented programming is about creating objects in code and making it appear on a screen. The objects are assigned properties and behaviors and have characteristics. When creating objects in programming, the programmer creates with some amount of abstraction. Things, persons, behaviors, objects etc. are complex and dynamic and cannot be exactly and 100 percent accurate be described. Codes demand that everything is described and defined, if not, the code is not working. Therefore, when creating objects in programs, the programmer has to define undefined elements. Furthermore, as real-world objects are complex, the programmer chooses what to include when creating objects in programs, and what to leave out. The consequence of this is that the objects created in programming always will be an abstraction of what the object represent. 

This level of abstraction is something to be aware of and think through when programming. Abstracting objects programmers must be aware of the elements programed and the elements left out. What image is the abstraction making? What political and cultural opinion is created in this abstraction? 


•  **Extend/connect your game project to wider digital culture context, can you think of a digital example and describe how complex details and operations are being abstracted?**

I created a very abstract and simple shape of a bomb. The object I programmed is very simple and without much detail, I therefore left out many feature of the original object. Furthermore, the bomb I decided to make was already an abstraction of a bomb. I tried to make a bomb as we see them in cartoons. The image of a round bomb with a little rectangle on top. By making this choice to recreate a cartoon bomb, I kept this image alive of a bomb looking like this, although a bomb in 2020 real life does not look like this. This was a clear cultural statement, keeping the already existing abstraction of a bomb alive and further abstracting it in my own work. 


![ScreenShot](Screenminiex6.png)

**URL:**  https://signe.tvilling.gitlab.io/ap-2020/Minix6/
