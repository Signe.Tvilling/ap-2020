let ToddSize = {
  w:75,
  h:75
};
var moveleft = 10
var moveright = 10
var toddposX= 689;
var toddposY= 556;
let obstacle = [];
let min_obstacle = 40;
let big=0
let lives = 3;
let score = 0;
let scoreminus = 0;
var k= 0;
let redbull = [];
let min_redbull = 4

function preload() {
  Todd = loadImage("Todd.png");
  sky = loadImage("sky3.jpg");
  fire = loadImage("ild.png");
  redbulls = loadImage("red_bull.png");
  ele = loadSound("Todd.mp3");

}
function setup() {
createCanvas(windowWidth, windowHeight);
//the background song
ele = createAudio('Todd.mp3');
//makes the audio play right away
ele.autoplay(true);

//generates the first 40 bombs
 for (let i=0; i<=min_obstacle; i++) {
   obstacle[i] = new Obstacle();
}
//generates the first 4 redbulls
  for (let i=0; i<=min_redbull; i++) {
     redbull[i] = new Redbull();
}
}
function draw() {
background(0);
//making the background move
k=k+1
image(sky, 0, k, windowWidth, windowHeight);
image(sky, 0, k-windowHeight, windowWidth, windowHeight);
image(sky, 0, k-2*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-3*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-4*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-5*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-6*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-7*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-8*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-9*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-10*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-11*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-12*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-13*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-14*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-15*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-16*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-17*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-18*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-19*windowHeight, windowWidth, windowHeight);
image(sky, 0, k-20*windowHeight, windowWidth, windowHeight);

//the player character
 image(fire, toddposX+10, toddposY+60, 50, 75);
 image(Todd,toddposX ,toddposY ,ToddSize.w ,ToddSize.h);

 //calling the functions i've made below
 screentext();
 showObstacle();
 checkObstacleNum();
 checkExplosion();
 showredbull();
 checkredbullNum();
 checkredbull();
}


function screentext() {
  //if statement used to show the text below in a limited amount of time
  if (frameCount > 0 && frameCount<240) {
     textAlign(CENTER);
     fill(255,0,0);
     textSize(30);
     text("The earth is burning", width/2, 175);
     text("Avoid the bombs and fly away", width/2, 300);

     textSize(40);
     fill(255,0,0);
     strokeWeight(4);
     stroke(207, 0, 0);
     textStyle(BOLD);
     text("Help Inspector Norse escape!", width/2, 235);
  }
  //the scoreboard in the left corner
    fill(150);
    noStroke();
    textSize(20);
    textAlign(LEFT);
    text("Lives left: "+lives, 70, 53);
    score = floor(frameCount/100) - scoreminus;
    text("Score: "+score, 70, 93)
}

 function showObstacle(){
   for (let i = 0; i <obstacle.length; i++) {
     obstacle[i].show();
     obstacle[i].move();
     obstacle[i].delete();
   }
  }

  function showredbull(){
    for (let i = 0; i <redbull.length; i++) {
      redbull[i].show();
      redbull[i].move();
      redbull[i].delete();
    }
   }
   

  // function used to generate new bombs
  function checkObstacleNum() {
    if (obstacle.length < min_obstacle) {
      obstacle.push(new Obstacle());
    }
  }

  // function used to generate new redbulls
  function checkredbullNum() {
    if (redbull.length < min_redbull) {
      redbull.push(new Redbull());
    }
  }

  //function making the explosion when the player hits a bomb
  function checkExplosion() {
  for (let i = 0; i <obstacle.length; i++) {
    //the distance from the player to the bombs
    let d = int(dist(toddposX+37, toddposY+37, obstacle[i].pos.x, obstacle[i].pos.y));
    //what happens when the bomb hits the player
    if (d < ToddSize.h-37) {
      //function making the red explosion
      explosion();
      lives=lives-1
      scoreminus = scoreminus+10
      obstacle.splice(i,1);
      //if the player runs out of lives
    if (lives === 0) {
      textAlign(CENTER);
      textSize(100);
      fill(255,0,0);
      strokeWeight(6);
      stroke(207,0,0);
      text("G A M E   O V E R", width/2, height/2-100);
      noLoop();
      }
    }
  }
}

//function giving points and playing sound when the player hits a redbull
function checkredbull() {
for (let i = 0; i <redbull.length; i++) {
  let d = int(dist(toddposX+37, toddposY+37, redbull[i].pos2.x, redbull[i].pos2.y));
  if (d < ToddSize.h-37) {
    scoreminus = scoreminus-20
    redbull.splice(i,1);
    drinking();
    }
  }
}

//function used to move the player around
 function keyPressed() {
 if (keyCode === 37) {
toddposX=toddposX-moveleft

} else if (keyCode === 39) {
toddposX=toddposX+moveright

} else if (keyCode === 38) {
toddposY=toddposY-moveright

} else if (keyCode === 40) {
toddposY = toddposY+moveright
 }
}

function drinking() {
  drink = createAudio('drink2.m4a');
  drink.autoplay(true);
}

function explosion() {
  stroke(242, 109, 0);
  fill(255,0,0);
  big=big+10
  ellipse(toddposX+37, toddposY+37,10,10);
  star(toddposX+37, toddposY+37, 40+big, 60+big, 40);
  star(toddposX+37-40, toddposY+37, 40+big, 100+big, 40);
  expl = createAudio('Explosion.mp3');
  expl.autoplay(true);
}

//the class making the bomb
class Obstacle {
constructor() {
  translate(-width/2,-height/2);
    this.speed = floor(random(2,5));
    this.pos = new createVector(random(10, width-10), -height+5);
    this.size = floor(random(15,35));
    this.bombtopsize = this.size/1.5;
}
    move() {
       this.pos.y+=this.speed;
}

  show() {
    noStroke();
    fill(100);
    ellipse(this.pos.x, this.pos.y, this.size, this.size);
    rect(this.pos.x-5, this.pos.y-this.size+5, this.bombtopsize/2, this.bombtopsize-5);
  }

//deleting the bombs when they disappear from the screen
  delete() {
    for (let i = 0; i <obstacle.length; i++) {
    if (obstacle[i].pos.y > 820) {
      obstacle.splice(i,1);
   }
  }
 }
}

//the class making the redbulls
class Redbull {
 constructor() {
   this.speed2 = floor(random(2,5));
   this.pos2 = new createVector(random(10, width-10), -height+5);
   this.size2 = floor(random(15,25));
 }

 move() {
    this.pos2.y+=this.speed2;
}

 show() {
   image(redbulls, this.pos2.x, this.pos2.y, this.size2, this.size2*2);
 }

 delete() {
   for (let i = 0; i <redbull.length; i++) {
   if (redbull[i].pos2.y > 820) {
     redbull.splice(i,1);
   }
  }
 }
}


//the shape of the explosion
function star(x, y, radius1, radius2, npoints) {
  let angle = TWO_PI / npoints;
  let halfAngle = angle / 2.0;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius2;
    let sy = y + sin(a) * radius2;
    vertex(sx, sy);
    sx = x + cos(a + halfAngle) * radius1;
    sy = y + sin(a + halfAngle) * radius1;
    vertex(sx, sy);
  }
  endShape(CLOSE);

}
